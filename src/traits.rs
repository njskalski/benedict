use crate::any_msg::AnyMsg;

pub enum Input {
    A,
    B
}

pub trait View<'a> {
    fn on_input(&self, input : Input) -> Option<Box<dyn AnyMsg>>;
    fn state(&mut self) -> &mut dyn State;
}

pub trait State {
    fn view(&self) -> Box<dyn View<'_> + '_> where Self : Sized;
    fn update(&mut self, msg : Box<dyn AnyMsg>) -> Option<Box<dyn AnyMsg>>;
}