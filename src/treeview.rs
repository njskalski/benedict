use std::borrow::BorrowMut;
use std::fmt::{Debug, Formatter};
use crate::{AnyMsg, Input};
use crate::traits::{State, View};

pub struct TreeViewState {

}

impl Default for TreeViewState {
    fn default() -> Self {
        TreeViewState {}
    }
}

#[derive(Debug)]
pub enum TreeViewMsg {

}

impl AnyMsg for TreeViewMsg {

}

pub struct TreeViewView<'a> {
    state : &'a TreeViewState,
}

impl<'a> View<'a> for TreeViewView<'a> {
    fn on_input(&self, input: Input) -> Option<Box<dyn AnyMsg>> {
        todo!()
    }

    fn state(&mut self) -> &mut dyn State {
        &mut self.state
    }
}

impl State for TreeViewState {
    fn view(&self) -> Box<dyn View<'_> + '_> where Self: Sized {
        Box::new(TreeViewView {
            state: self
        })
    }

    fn update(&mut self, msg: Box<dyn AnyMsg>) -> Option<Box<dyn AnyMsg>> {
        todo!()
    }
}