use std::borrow::BorrowMut;
use std::fmt::{Debug, Formatter};
use crate::{AnyMsg, Input};
use crate::traits::{State, View};
use crate::treeview::{TreeViewState, TreeViewView};

pub struct MainViewState {
    tree : TreeViewState
}

impl Default for MainViewState {
    fn default() -> Self {
        MainViewState {
            tree : TreeViewState::default()
        }
    }
}

#[derive(Debug)]
pub enum MainViewMsg {

}

impl AnyMsg for MainViewMsg {

}

struct MainViewView<'a> {
    state : &'a MainViewState,
}


impl<'a> View<'a> for MainViewView<'a> {
    fn on_input(&self, input: Input) -> Option<Box<dyn AnyMsg>> {
        todo!()
    }

    fn state(&mut self) -> &mut dyn State {
        &mut self.state as &mut dyn State
    }
}

impl State for MainViewState {
    fn view(&self) -> Box<dyn View<'_> + '_> where Self: Sized {
        let tree_view = self.tree.view();

        Box::new(MainViewView {
            state: self
        })
    }

    fn update(&mut self, msg: Box<dyn AnyMsg>) -> Option<Box<dyn AnyMsg>> {
        todo!()
    }
}